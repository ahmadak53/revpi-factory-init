# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Callable
import re
import logging
import sys

logger = logging.getLogger(__name__)


DEFAULT_ERROR_MESSAGE = "Your MAC address should resemble six little chunks, kind of like 12:34:56:78:9A:BC. Please double-check."


def validate_mac_address(
    mac_address: str,
    error_callback: Callable,
    error_message: str = DEFAULT_ERROR_MESSAGE,
) -> None:
    if re.match(r"^[0-9a-f]{12}$", mac_address) is None:
        error_callback(error_message)


def parse_mac_address(mac: str) -> str:
    return mac.replace(":", "").replace("-", "").lower()


def replace_in_file(pattern: str, replacement: str, file_path: str) -> None:
    """
    Replace all occurrences of a given pattern with a specified replacement in a file.

    Args:
        pattern (str): The regular expression pattern to search for.
        replacement (str): The string to replace matches with.
        file_path (str): The path to the file in which replacements should be made.

    Returns:
        None

    Note:
        This function reads the entire file into memory, so it might not be suitable for very large files.
    """
    try:
        with open(file_path, "r") as file:
            lines = file.readlines()
        with open(file_path, "w") as file:
            for line in lines:
                file.write(re.sub(pattern, replacement, line))
    except FileExistsError:
        logger.error("could not open file '%s' for writing", file_path)
        sys.exit(1)
