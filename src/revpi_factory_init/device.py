# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later


device_eth_count = {"core": 1, "compact": 2, "connect": 2, "connect-se": 2, "flat": 2}


class Device:
    def __init__(self, device_type: str, serial_number: str, mac_address: str):
        self.device_type = device_type
        self.serial_number = serial_number
        self.mac_address = mac_address
        self.eth_count = device_eth_count[device_type]
