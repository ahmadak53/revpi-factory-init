# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later
import argparse

from helper import parse_mac_address, validate_mac_address

from revpi_factory_init import __version__
from revpi_factory_init.logger import logger_set_level
from device import Device

device_list = ("compact", "connect", "core", "flat", "connect-se")


class Cli:
    def __init__(self):
        self._parser = argparse.ArgumentParser(
            prog="revpi-factory-init",
            description="""initialize dtoverlay, password of user pi, machine-id,
                                                    hostname, FQDN and MAC address of a RevPi to factory defaults""",
        )

    def parse_args(self) -> argparse.Namespace:
        self._parser.add_argument(
            "device_type",
            help=f"device type to configure the overlay. Choose one from: {', '.join(device_list)}",
        )
        self._parser.add_argument(
            "serial_number",
            help="serial number, which is located on the bottom of the front of the device.",
            type=int,
        )
        self._parser.add_argument(
            "mac_address",
            help="MAC address, which is located on the middle of the front of the device.",
            type=parse_mac_address,
        )

        self._parser.add_argument(
            "--version",
            action="version",
            dest="version",
            help="display version info and exit",
            version=f"%(prog)s {__version__}",
        )

        self._parser.add_argument(
            "--verbose",
            "-v",
            dest="verbose",
            help="show more information",
            action="store_true",
        )

        return self._parser.parse_args()

    def validate_args(self, args: argparse.Namespace) -> None:
        if args.device_type not in device_list:
            self._parser.error(
                f"{args.device_type} is not a valid device type. Choose one from: {', '.join(device_list)}"
            )

        if args.verbose:
            logging_level = "INFO"
        else:
            logging_level = "ERROR"

        logger_set_level(logging_level)

        validate_mac_address(args.mac_address, self._parser.error)

    def run(self) -> Device:
        args = self.parse_args()
        self.validate_args(args)
        return Device(
            device_type=args.device_type,
            serial_number=str(args.serial_number),
            mac_address=args.mac_address,
        )
