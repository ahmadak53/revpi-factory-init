# SPDX-FileCopyrightText: 2023 KUNBUS GmbH
#
# SPDX-License-Identifier: GPL-2.0-or-later

import socket
import subprocess
import sys
import logging
from revpi_factory_init.helper import replace_in_file
from revpi_factory_init.device import Device


logger = logging.getLogger(__name__)


class RevPiInitManager:
    def __init__(self, device_var: Device):
        self._piSerial_cmd = "/usr/sbin/piSerial"
        self._chpasswd_cmd = "/usr/sbin/chpasswd"
        self._dtoverlay_cmd = "/usr/bin/dtoverlay"
        self._host_file = "/etc/hosts"
        self._config_file = "/boot/config.txt"
        self._device_var = device_var

    def _run(
        self,
        cmd,
        *args,
        cmd_input=None,
    ) -> subprocess.CompletedProcess[bytes] | None:
        try:
            process = subprocess.run(
                [cmd, *args], capture_output=True, input=cmd_input, check=True
            )
            return process
        except subprocess.CalledProcessError as e:
            logger.error("Failed to execute command %s due to error: %s", cmd, e)
            sys.exit(1)

    @property
    def passwd(self):
        logger.info("Retrieving password using %s", self._piSerial_cmd)
        return self._run(self._piSerial_cmd, "-p").stdout.decode()

    @passwd.setter
    def passwd(self, new_pwd):
        logger.info("Setting new password")
        self._run(self._chpasswd_cmd, cmd_input=f"pi:{new_pwd}".encode())

    def set_hostname(self) -> None:
        logger.info("Setting hostname")
        hostname = f"RevPi{self._device_var.serial_number}"
        new_hostname = f"127.0.1.1\t{hostname}\n"
        replace_in_file(r"^127\.0\.1\.1[\t]+RevPi[0-9]*", new_hostname, self._host_file)
        socket.sethostname(hostname)

    def dtoverlay(self) -> None:
        logger.info("Applying device overlay")
        self._run(self._dtoverlay_cmd, f"revpi-{self._device_var.device_type}")

    def dt_eth_param(self) -> None:
        mac_address = self._device_var.mac_address
        # Split the MAC address into high and low parts
        mac_hi = f"0x{mac_address[:8]}"
        mac_lo = f"0x{mac_address[8:]}"
        # Remove any existing MAC address parameters from the file
        replace_in_file(r"^dtparam=eth[0-9]_mac_.*(\n)?", "", self._config_file)
        with open(self._config_file, "a") as f:
            for i in range(self._device_var.eth_count):
                f.write(f"dtparam=eth{i}_mac_hi={mac_hi}\n")
                # Calculate and write the low part of the MAC address for the current interface
                # The low part is incremented for each interface
                f.write(f"dtparam=eth{i}_mac_lo={hex(int(mac_lo,16) + i)}\n")
